onEvent('item.right_click', event => {
  if (event.item === 'minecraft:glass_bottle' && event.player.y >= 128) {
    event.player.give("kubejs:bottled_air")
    event.item.count -= 1
  }
})

// autocrafting
let DispenserBlock = java("net.minecraft.world.level.block.DispenserBlock")
DispenserBlock.registerBehavior(Item.of("minecraft:glass_bottle").item,
    (blockSource, itemStack) => {
        let direction = blockSource.getBlockState().getValue(DispenserBlock.FACING)
        let entity = blockSource.getEntity()
        let block = Utils.getLevel(blockSource.getLevel()).getBlock(blockSource.getPos())

        if (blockSource.y() < 128) {
            let copy = itemStack.copy()
            itemStack.shrink(1)
            copy.setCount(1)
            block.popItemFromFace(copy, direction)
            return itemStack;
        }

        let outItem = Item.of("kubejs:bottled_air").itemStack;

        itemStack.shrink(1)
        if (itemStack.empty) {
            return outItem;
        } else {
            if (entity.addItem(outItem.copy()) < 0)
                block.popItemFromFace(outItem, direction)
            return itemStack
        }
    })
